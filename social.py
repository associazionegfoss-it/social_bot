# -*- coding: utf-8 -*-

"""
***************************************************************************
    social.py adattamento del codice qgisita_fb_twitter.py per
    l'associazione gfoss.it
    ---------------------
    Date                 : January 2021
    Copyright            : (C) 2017, 2021
    Email                : lrssvtml at gmail dot com
                           lucadeluge at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Salvatore Larosa, Luca Delucchi'
__date__ = 'January 2021'
__copyright__ = '(C) 2017, 2021'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'
from configparser import ConfigParser
import os
import requests
import twitter
import json
import copy
from facepy import GraphAPI
import sqlite3

#read config file
config = ConfigParser()
config.read('settings.ini')

#get access info for facebook
FB_USER_TOKEN = config.get('FACEBOOK', 'USER_TOKEN')
FB_PAGE_ID = config.get('FACEBOOK', 'PAGE_ID')
FB_PAGE_URL = config.get('FACEBOOK', 'PAGE_URL')
#get access info for twitter
API_TWITTER = twitter.Api(consumer_key=config.get('TWITTER', 'CONSUMER_KEY'),
                          consumer_secret=config.get('TWITTER',
                                                     'CONSUMER_SECRET'),
                          access_token_key=config.get('TWITTER', 'TOKEN_KEY'),
                          access_token_secret=config.get('TWITTER',
                                                         'TOKEN_SECRET'))
#get access info for telegram
TELEGRAM_BOT_TOKEN = config.get('TELEGRAM', 'TOKEN')
TELEGRAM_METHOD = 'sendMessage'
TELEGRAM_CHAT_ID = config.get('TELEGRAM', 'ID')
#get access info for linkedin
LINKEDIN_ID = config.get('LINKEDIN', 'ID')
LINKEDIN_TOKEN = config.get('LINKEDIN', 'TOKEN')

SQLITE_DB = config.get('DATABASE', 'SQLITE_PATH')

TAGS_PATH = config.get("GENERAL", "TAGS")

if os.path.exists(TAGS_PATH):
    with open(TAGS_PATH) as f:
        TAGS = json.load(f)
else:
    TAGS = {}

def get_old_posts(conn):
    """Get old posts from database"""
    data = conn.execute('SELECT id from facebook_posts;').fetchall()
    return [i[0] for i in data]

def insert_post(conn, pid):
    """Insert the new post"""
    conn.execute('INSERT INTO facebook_posts VALUES (?)', (pid,))

def replace(message, social):
    new = copy.deepcopy(message)
    for key in TAGS.keys():
        if key in message:
            if social in TAGS[key].keys():
                new = new.replace(key, TAGS[key][social])
    return new

def post_list(limit=5):
    graph = GraphAPI(FB_USER_TOKEN)
    query_string = "?fields=posts.limit({val})".format(val=limit)
    query_string += "{attachments,id,message,full_picture}"
    datas = graph.get('/v4.0/' + FB_PAGE_ID + query_string, page=False,
                      retry=5)
    del graph
    return datas['posts']['data']

def _publish_twitter(message, full_picture, id_post):
    try:
        message = replace(message, 'twitter')
        status = API_TWITTER.PostUpdate(message, media=full_picture,
                                        verify_status_length=False)
    except Exception as e:
        if e.message[0]['code'] == 186:
            message = message[:200] + '... ' + FB_PAGE_URL + id_post.split('_')[1]
            message = replace(message, 'twitter')
            status = API_TWITTER.PostUpdate(message, media=full_picture)
    return status

def _publish_telegram(message, parsemode='HTML'):
    post_data = {'chat_id': TELEGRAM_CHAT_ID, 'text': message,
                 'parse_mode': parsemode, 'disable_web_page_preview': 'false'}
    response = requests.post(
        url = 'https://api.telegram.org/bot{0}/{1}'.format(TELEGRAM_BOT_TOKEN,
                                                           TELEGRAM_METHOD),
        data = post_data
    ).json()
    return response

def _publish_linkedin(message, full_picture, id_post, linktype="NONE",
                      description=None, title=None):
    url = "https://api.linkedin.com/v2/ugcPosts"
    headers = {'Content-Type': 'application/json',
               'X-Restli-Protocol-Version': '2.0.0',
               'Authorization': 'Bearer ' + LINKEDIN_TOKEN}
    post_data = {
        "author": "urn:li:organization:" + LINKEDIN_ID,
        "lifecycleState": "PUBLISHED",
        "specificContent": {
            "com.linkedin.ugc.ShareContent": {
                "shareCommentary": {
                    "text": replace(message, 'linkedin')
                },
                "shareMediaCategory": linktype
            }
        },
        "visibility": {
            "com.linkedin.ugc.MemberNetworkVisibility": "PUBLIC"
        }
    }
    if linktype == "ARTICLE":
        if not description:
            print("Description is missing")
            return False
        if not title:
            print("Title is missing")
            return False
        media = [
            {
                "status": "READY",
                "description": {
                    "text": description
                },
                "originalUrl": FB_PAGE_URL + id_post.split('_')[1],
                "title": {
                    "text": title
                }
            }
        ]
        post_data["com.linkedin.ugc.ShareContent"]["media"] = media

    response = requests.post(url, headers=headers, json=post_data).json()
    return response

def send_request():
    conn = sqlite3.connect(SQLITE_DB)
    cur = conn.cursor()
    old_posts = get_old_posts(cur)
    posts = post_list()
    post = posts[0]
    if post['id'] not in old_posts:
        id_post = post['id']
        try:
            full_picture = post['full_picture']
        except KeyError:
            full_picture = ''

        try:
            source = post['media']
            has_media = True
        except KeyError:
            source = ''
            has_media = False

        try:
            attachment = post['attachments']
            title = attachment['data'][0]['title']
            title = '' if title == u'Timeline Photos' else title
            has_attachment = True
        except KeyError:
            has_attachment = False
            title = ''

        try:
            if has_attachment:
                sub_attachent = post['attachments']['data'][0]['subattachments']
                has_sub_attachments = True
        except KeyError:
            has_sub_attachments = False

        try:
            description = post['description']
        except KeyError:
            description = ''

        try:
            message = post['message']
            has_message = True
        except KeyError:
            message = ''
            has_message = False
        if has_message and not has_attachment:
            _publish_twitter(message, full_picture, id_post)
            thismedia = [source if has_media else full_picture][0]
            teletext = '{me}\n\n<a href="{tm}">  </a>'.format(me=message,
                                                              tm=thismedia)
            _publish_telegram(teletext)
            _publish_linkedin(message, full_picture, id_post)
        elif has_message and has_attachment:
            _publish_twitter(message, full_picture, id_post)
            teletext = '{me}\n{de}\n\n{ti}\n\n<a href="{at}">[' \
                       'Visualizza contenuto]</a>'.format(me=message,
                                                          de=description,
                                                          ti=title,
                                                          at=attachment['data'][0]['url'])
            _publish_telegram(teletext)
            _publish_linkedin(message, full_picture, id_post)
        else:
            if 'cover photo' not in title:
                if full_picture:
                    _publish_twitter("{ti} {fp}".format(ti=title,
                                                        fp=full_picture),
                                     None, id_post)
                else:
                    _publish_twitter("{ti} {fp}".format(ti=description,
                                                        fp=title),
                                     None, id_post)
                teletext = description + '\n' + title + \
                           '\n[Visualizza contenuto](' + attachment['data'][0]['url'] + ')'
                _publish_telegram(teletext, 'Markdown')
                _publish_linkedin(message, full_picture, id_post)
                if has_sub_attachments:
                    TELEGRAM_METHOD = 'sendMediaGroup'
                    photos = []
                    for i in range(0, len(sub_attachent['data'])):
                        photos.append(sub_attachent['data'][i]['media']['image']['src'])
                    media_f = []
                    for i, f in enumerate(photos):
                        media_f.append({
                            "type": "photo",
                            "media": photos[i]
                        })
                    requests.post(
                        url='https://api.telegram.org/bot{0}/{1}'.format(TELEGRAM_BOT_TOKEN, TELEGRAM_METHOD),
                        data={'chat_id': TELEGRAM_CHAT_ID,
                              'media': json.dumps(media_f)}
                    ).json()

        insert_post(cur, id_post)
        conn.commit()
        conn.close()
    else:
        print('No new posts')

def main():
    if not os.path.exists(SQLITE_DB):
        conn = sqlite3.connect(SQLITE_DB)
        cur = conn.cursor()
        cur.execute('''create table facebook_posts(id text UNIQUE)''')
        conn.commit()
        conn.close()
    send_request()

if __name__ == '__main__':
    main()
